package nl.patrickdevelopment.studiebarometer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;


import org.w3c.dom.Text;

import java.util.List;

import nl.patrickdevelopment.studiebarometer.controller.CourseController;
import nl.patrickdevelopment.studiebarometer.model.Course;


public class MainActivity extends AppCompatActivity {

    private String strUserName;
    private ListView mListView;
    private TextView TVStudentName, TVEC;
    private Spinner SPeriod;

    private CourseListAdapter mAdapter;
    private Gson gson = new Gson();
    private CourseController CourseCtl;
    List<Course> courses;
    static final int SETTINGS_REQUEST = 1;  // The request code
    private SharedPreferences SP;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Heeft de applicatie een internet verbinding ?
        if (!DetectConnection.checkInternetConnection(this)) {
            Toast.makeText(getApplicationContext(), R.string.toast_no_internet_connection, Toast.LENGTH_SHORT).show();
        }

        //init elements from view
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mListView       = (ListView) findViewById(R.id.LvCourses);
        TVStudentName   = (TextView) findViewById(R.id.textview_main_student_name);
        SPeriod         = (Spinner)  findViewById(R.id.spinner_main_period);
        TVEC            = (TextView) findViewById(R.id.textview_main_ec_amount);

        this.fillCourseList();

        SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        //set username
        this.setUsername(); //check if username is set

        //fill spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.Period_arrays));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPeriod.setAdapter(dataAdapter);

        //set spinner postiton
        SPeriod.setSelection(SP.getInt("PeriodArrayLocation", 0));

        //set on select listener
        SPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 4) {
                    Log.d("save","spinner");
                    SharedPreferences.Editor editor = SP.edit();
                    editor.putInt("PeriodArrayLocation", position).commit();
                }
                switch (position) {
                    case 4:
                        startActivity(new Intent(MainActivity.this, Overzicht_EC.class));
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });

        TVEC.setText(CourseCtl.getTotalETC() + " / 60");

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView subject = (TextView) view.findViewById(R.id.subject_name);
                //start detail activity
                Intent detailViewIntent = new Intent(MainActivity.this, CourseDetailActivity.class);
                detailViewIntent.putExtra("subject", subject.getText().toString());
                startActivityForResult(detailViewIntent,999);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Get the functions and update here
        this.fillCourseList();
        this.setUsername();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(MainActivity.this, SettingsActivity.class), SETTINGS_REQUEST);
            return true;
        }
        if (id == R.id.action_overview) {
            startActivityForResult(new Intent(MainActivity.this, Overzicht_EC.class), SETTINGS_REQUEST);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * check if username is set
     */
    public void setUsername() {

        if (SP.getString("name", null) == null) {
            startActivityForResult(new Intent(MainActivity.this, SettingsActivity.class), SETTINGS_REQUEST);
            strUserName = "Student";
        } else {
            strUserName = SP.getString("name", null);
        }
        this.TVStudentName.setText(strUserName);

    }

    /**
     * fill the courselist adapter and update the list
     */
    private void fillCourseList() {
        CourseCtl = new CourseController(getApplicationContext());
        courses = CourseCtl.getCourses();

        List<Course> courseList = CourseCtl.getCourses();
        mAdapter = new CourseListAdapter(MainActivity.this, 0, courseList);
        mListView.setAdapter(mAdapter);

        TVEC.setText(CourseCtl.getTotalETC() + " / 60");
    }
}