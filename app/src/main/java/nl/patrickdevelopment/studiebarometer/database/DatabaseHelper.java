package nl.patrickdevelopment.studiebarometer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by Patrick on 2-3-2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;			// SINGLETON TRUC
    public static final String dbName = "barometer.db";	// Naam van je DB
    public static final int dbVersion = 3;				// Versie nr van je db.


    public DatabaseHelper(Context ctx) {				// De constructor doet niet veel meer dan ...
        super(ctx, dbName, null, dbVersion);			// … de super constructor aan te roepen.
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version ){
        super(context,name,factory, version);
    }

    /**
     * check if instance exist and give active instance back
     * @param ctx
     * @return
     */
    public static synchronized DatabaseHelper getHelper (Context ctx){  // SYNCRONIZED TRUC
        if (mInstance == null){
            mInstance = new DatabaseHelper(ctx);
            mSQLDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    /**
     * create new database if not exist
     * @param db
     */
    @Override // CREATE TABLE course (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, ects TEXT, code TEXT grade TEXT);
    public void onCreate(SQLiteDatabase db) {
        Log.d("database","created");
        db.execSQL("CREATE TABLE " + DatabaseInfo.CourseTables.COURSE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.CourseColumn.NAME + " TEXT," + DatabaseInfo.CourseColumn.ECTS + " INTEGER," +
                        DatabaseInfo.CourseColumn.GRADE + " TEXT," + DatabaseInfo.CourseColumn.PERIOD + " INTEGER,"+
                        DatabaseInfo.CourseColumn.CHANCE + " INTEGER DEFAULT 1"+");");
    }

    /**
     * if database version is verhoogd
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {	 // BIJ EEN UPDATE VAN DE DB (ID verhoogd)
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.CourseTables.COURSE);	 // GOOI ALLES WEG
        onCreate(db);									 // EN CREER HET OPNIEUW
    }


    /**
     * insert command for database
     * @param table tableName
     * @param nullColumnHack where colloms
     * @param values values for insert
     */
    public void insert(String table, String nullColumnHack, ContentValues values){
        mSQLDB.insert(table, nullColumnHack, values);
    }

    /**
     * query
     * @param table
     * @param columns selected colums
     * @param selection colums where
     * @param selectArgs values where
     * @param groupBy group by colum
     * @param having having colum
     * @param orderBy order by collum
     * @return
     */
    public Cursor query(String table,String[] colums){
        return mSQLDB.query(table, colums , null, null, null, null, null);
    }

    /**
     * query
     * @param table
     * @param columns selected colums
     * @param selection colums where
     * @param selectArgs values where
     * @param groupBy group by colum
     * @param having having colum
     * @param orderBy order by collum
     * @return
     */
    public Cursor query(String table, String[] columns, String selection, String[] selectArgs, String groupBy, String having, String orderBy){
        return mSQLDB.query(table, columns, selection, selectArgs, groupBy, having, orderBy);
    }

    /**
     * insert command for database
     * @param table tableName
     * @param values values for update
     * @param selection colums where
     * @param selectArgs values where
     */
    public void update(String table, ContentValues values, String selection, String[] selectArgs){
        mSQLDB.update(table, values, selection, selectArgs);
    }

    /**
     * insert command for database
     * @param table tableName
     * @param selection colums where
     * @param selectArgs values where
     */
    public void delete(String table, String selection, String[] selectArgs){
        mSQLDB.delete(table, selection, selectArgs);
    }

}


