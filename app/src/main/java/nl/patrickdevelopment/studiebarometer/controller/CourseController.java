package nl.patrickdevelopment.studiebarometer.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.patrickdevelopment.studiebarometer.database.DatabaseHelper;
import nl.patrickdevelopment.studiebarometer.database.DatabaseInfo;
import nl.patrickdevelopment.studiebarometer.model.Course;

/**
 * Created by Patrick on 2-3-2016.
 */
public class CourseController extends Controller {

    private List<Course> Courses;
    private Context context;
    private DatabaseHelper DatabaseHelper;
    protected String[] allColumns = {BaseColumns._ID, DatabaseInfo.CourseColumn.NAME, DatabaseInfo.CourseColumn.ECTS, DatabaseInfo.CourseColumn.GRADE, DatabaseInfo.CourseColumn.PERIOD, DatabaseInfo.CourseColumn.CHANCE};

    public CourseController(Context context) {
        this.context = context;
        DatabaseHelper = new DatabaseHelper(context).getHelper(context);
    }

    public List<Course> getCourses() {
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, null, null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return this.CursorToCourseList(c);
    }

    public List<Course> CursorToCourseList(Cursor cursor) {
        this.Courses = new ArrayList<Course>();

        cursor.moveToFirst();

        //if just 1
        if (cursor.getCount() == 1) {
            this.Courses.add(new Course(cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getInt(4),cursor.getInt(5)));
        } else {
            while (cursor.moveToNext()) {
                this.Courses.add(new Course(cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getInt(4),cursor.getInt(5)));
            }
        }
        return this.Courses;
    }

    public void saveCourse(Course course) {
        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.CourseColumn.NAME, course.getName());
        values.put(DatabaseInfo.CourseColumn.ECTS, course.getEcts());
        values.put(DatabaseInfo.CourseColumn.GRADE, course.getGrade());
        values.put(DatabaseInfo.CourseColumn.PERIOD, course.getPeriod());
        values.put(DatabaseInfo.CourseColumn.CHANCE, course.getChance());
        DatabaseHelper.insert(DatabaseInfo.CourseTables.COURSE, null, values);
    }

    public void GetCourseByID(int ID) {

    }

    public List<Course> getCourseByName(String Name) {
        String[] whereArgs = {Name};
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, DatabaseInfo.CourseColumn.NAME + " =?", whereArgs, null, null, null);
        return this.CursorToCourseList(c);
    }

    public void UpdateCourse(Course course) {

        String[] whereArgs = {course.getName()};

        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.CourseColumn.ECTS, course.getEcts());
        values.put(DatabaseInfo.CourseColumn.GRADE, course.getGrade());
        values.put(DatabaseInfo.CourseColumn.PERIOD, course.getPeriod());
        values.put(DatabaseInfo.CourseColumn.CHANCE, course.getChance());
        DatabaseHelper.update(DatabaseInfo.CourseTables.COURSE,values, DatabaseInfo.CourseColumn.NAME+"=?", whereArgs );
    }

    public void deleteCourse(Course course) {
        ContentValues values = new ContentValues();
        String[] params = new String[]{};
        DatabaseHelper.delete(DatabaseInfo.CourseTables.COURSE, DatabaseInfo.CourseColumn.NAME + "='" + course.getName() + "'" , params);
    }

    public List<Course> getCompletedCourses() {
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, DatabaseInfo.CourseColumn.GRADE + " > 5.5", null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return this.CursorToCourseList(c);
    }

    public List<Course> getCompletedCourses(Integer period) {
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, DatabaseInfo.CourseColumn.GRADE + " > 5.5 AND " + DatabaseInfo.CourseColumn.PERIOD + " <= " + String.valueOf(period), null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return this.CursorToCourseList(c);
    }

    public List<Course> getFailedCourses() {
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, DatabaseInfo.CourseColumn.GRADE + " < 5.5", null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return this.CursorToCourseList(c);
    }

    public List<Course> getFailedCourses(Integer period) {
        Log.d("where statement", DatabaseInfo.CourseColumn.GRADE + " > 5.5 AND " + DatabaseInfo.CourseColumn.PERIOD + " <= " + String.valueOf(period));
        Cursor c = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, allColumns, DatabaseInfo.CourseColumn.GRADE + " < 5.5 AND " + DatabaseInfo.CourseColumn.PERIOD + " <= " + String.valueOf(period), null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return this.CursorToCourseList(c);
    }

    public Integer getTotalETC() {
        Integer Ect = 0;

        Cursor cursor = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, DatabaseInfo.CourseColumn.GRADE + " > 5.5 ", null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");

        cursor.moveToFirst();

        //if just 1
        if (cursor.getCount() == 1) {
            Ect += cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
        } else {

            // For all the items we get in the return
            while (!cursor.isAfterLast()) {
                Ect += cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
                cursor.moveToNext();
            }
        }


        Log.d("miyvrey", Ect + "");
        return Ect;
    }

    public int getCompletedKernCourses() {
        Cursor cursor = DatabaseHelper.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, DatabaseInfo.CourseColumn.NAME + " IN ('INET', 'IOPR1', 'IOPR2', 'IRDB') AND "+DatabaseInfo.CourseColumn.GRADE + " > 5.5", null, null, null, DatabaseInfo.CourseColumn.PERIOD + " ASC");
        return cursor.getCount();
    }

}
