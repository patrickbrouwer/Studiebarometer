package nl.patrickdevelopment.studiebarometer;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import nl.patrickdevelopment.studiebarometer.model.Course;

/**
 * Created by Patrick on 9-3-2016.
 */
public class CourseListAdapter extends ArrayAdapter<Course> {

    public CourseListAdapter(Context context, int resource, List<Course> objects){
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;

        if (convertView == null ) {
            vh = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.view_content_row, parent, false);
            vh.name = (TextView) convertView.findViewById(R.id.subject_name);
            vh.code = (TextView) convertView.findViewById(R.id.subject_code);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Course cm = getItem(position);
        vh.name.setText(cm.getName());

        // Als het geen voldoende is mag de text rood worden
        if(cm.getCompleet() == false) {
            vh.name.setTextColor(Color.RED);
        }
        // Als het een voldoende is mag de text Zwart blijven
        if(cm.getCompleet() == true) {
            vh.name.setTextColor(Color.BLACK);
        }
        // Onder het vak, de Periode, Aanstal studiepunten en het Cijfer laten zien
        vh.code.setText("Periode: "+cm.getPeriod() +" | ECTS: "+ cm.getEcts() +" | Cijfer: "+ cm.getGrade() );

        // Echo getEcts voor debugging
        return convertView;
    }


    private static class ViewHolder {
        TextView name;
        TextView code;

    }
}

