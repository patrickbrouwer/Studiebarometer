package nl.patrickdevelopment.studiebarometer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.List;

import nl.patrickdevelopment.studiebarometer.Volley.VolleyHelper;
import nl.patrickdevelopment.studiebarometer.controller.CourseController;
import nl.patrickdevelopment.studiebarometer.model.Course;

public class SplashScreenActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private SharedPreferences SP;
    private String username;
    private String usernumber;

    private CourseListAdapter mAdapter;
    private ListView mListView;
    private Gson gson = new Gson();
    private CourseController CourseCtl;
    RequestQueue requestQueue;
    List<Course> courses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Shared preferences initial
        SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        // Lets get some interesting name of the user, if this has been set
//        if (SP.getString("name", null) == null) {
//            String username = SP.getString("name", null);
//            String strUserNumber = SP.getString("userNumber", null);
//
////            if(strUserNumber != "") {
////                if((strUserNumber.length() == 7)){
////                    usernumber = strUserNumber;
////                }
////            }
//        } else {
//            String username = "";
//            String usernumber = "";
//        }




        // Get the image
        ImageView myImageView = (ImageView)findViewById(R.id.imgLogo);

        // Set the animation on the image
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        myImageView.startAnimation(myFadeInAnimation);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over

                if (username != "") {
                    // Now lets go to the MainActivity because we dont need any login
                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    // Now lets go to the MainActivity because we dont need any login
                    Intent intent = new Intent(SplashScreenActivity.this, StartupActivity.class);
                    startActivity(intent);
                }


                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

        // Get here some interesting load of JSON
        //init courseController
        CourseCtl = new CourseController(getApplicationContext());
        courses = CourseCtl.getCourses();

        //check if have courses
            this.downloadCourseList();
    }

    /**
     * download the courses to the database
     */
    public void downloadCourseList() {
        Log.d("download", "courses");
        VolleyHelper volleyHelper = new VolleyHelper(getApplicationContext());
        JsonArrayRequest courseLoader = volleyHelper.getRequest(new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                for (Course courseInfo : gson.fromJson(response.toString(), Course[].class)) {
                    // First delete
                    CourseCtl.deleteCourse(courseInfo);

                    // Now safe
                    CourseCtl.saveCourse(courseInfo);
                }
            }
        });

        volleyHelper.addToRequestQueue(courseLoader);
    }
}
