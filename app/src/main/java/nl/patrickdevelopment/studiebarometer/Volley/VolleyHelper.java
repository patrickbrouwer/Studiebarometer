package nl.patrickdevelopment.studiebarometer.Volley;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import nl.patrickdevelopment.studiebarometer.controller.CourseController;

/**
 * Created by Patrick on 9-3-2016.
 */
public class VolleyHelper {
    private static VolleyHelper mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;
    private final String BASE_URL = "http://ipmedt.nl/imtpmd/api/grades/id/";

    private CourseController courseController;

    public VolleyHelper(final Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyHelper(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


    public JsonArrayRequest getRequest (Response.Listener<JSONArray> listener) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(mCtx);


        String schoolcode = SP.getString("userNumber", null);

        JsonArrayRequest courseLoader = new JsonArrayRequest(BASE_URL + schoolcode + "/", listener , this.errorListener());
        return courseLoader;
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", error.getMessage());
            }
        };
    }

}
