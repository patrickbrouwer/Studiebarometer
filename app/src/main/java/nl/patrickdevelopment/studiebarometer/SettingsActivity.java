package nl.patrickdevelopment.studiebarometer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import nl.patrickdevelopment.studiebarometer.controller.CourseController;

public class SettingsActivity extends AppCompatActivity {

    EditText ETName;
    EditText ETNumber;
    Button BSave;
    private TextInputLayout inputLayoutSchoolcode;
    private TextInputLayout inputLayoutUsername;
    RequestQueue requestQueue;
    String insertUrl = "http://ipmedt.nl/imtpmd/api/gebruikers/post/";
    private Gson gson = new Gson();
    private CourseController CourseCtl;
    private int enabled = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        ETName = (EditText) findViewById(R.id.ETName);
        ETNumber = (EditText) findViewById(R.id.ETNumber);
        BSave = (Button) findViewById(R.id.save);

        inputLayoutSchoolcode = (TextInputLayout) findViewById(R.id.input_layout_schoolcode);
        inputLayoutUsername = (TextInputLayout) findViewById(R.id.input_layout_username);

        requestQueue = Volley.newRequestQueue(getApplicationContext());



        //set values of the fields
        String strUserName = SP.getString("name",  null);
        String strUserNumber = SP.getString("userNumber",  null);
        ETName.setText(strUserName);
        ETNumber.setText(strUserNumber);

        if(strUserNumber != null){
            ETNumber.setEnabled(false);
            enabled = 0;
        }

        BSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
    }

    private void submitForm() {
        if (!validateSchoolcode() || !validateusername()) {
            return;
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("Response", response);

                            if (response.toLowerCase().contains("inserten is geslaagd")) {
                                onBackPressed();
                            } else if (response.toLowerCase().contains("Duplicate entry")) {
                                Log.d("Response", response);

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                                Log.e("ging wat fout", error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", SP.getString("name", null));
                    params.put("user_number", SP.getString("userNumber",  null));
                    return params;
                }

            }; requestQueue.add(stringRequest);

            if(enabled == 1){
                finishAffinity();
                Intent intent = new Intent(SettingsActivity.this, SplashScreenActivity.class);
                finish();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                String text = getString(R.string.personal_startup);
                Context context = SettingsActivity.this;
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                toast.show();
            }

            finish();

        }
    }

    private boolean validateusername() {
        String username = ETName.getText().toString().trim();

        if (!username.isEmpty()) {

            // if we have exact 8 items (and no spaces)
            if (username.length() > 2) {

                final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

                SharedPreferences.Editor editor = SP.edit();
                String value = ETName.getText().toString();
                editor.putString("name", value);
                editor.commit();

            } else {
                inputLayoutUsername.setError(getString(R.string.username_error));
                requestFocus(ETNumber);
                return false;
            }
        } else {
            String text = getString(R.string.username_empty);
            Context context = SettingsActivity.this;
            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
        return true;
    }

    private boolean validateSchoolcode() {
        String schoolcode = ETNumber.getText().toString().trim();

        if (!schoolcode.isEmpty()) {

            // if we have exact 8 items (and no spaces)
            if (schoolcode.length() == 7) {

                final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

                SharedPreferences.Editor editor = SP.edit();
                String value = ETNumber.getText().toString();
                editor.putString("userNumber", value);
                editor.commit();

            } else {
                inputLayoutSchoolcode.setError(getString(R.string.schoolcode_error));
                requestFocus(ETNumber);
                return false;
            }
        } else {
            String text = getString(R.string.schoolcode_empty);
            Context context = SettingsActivity.this;
            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
