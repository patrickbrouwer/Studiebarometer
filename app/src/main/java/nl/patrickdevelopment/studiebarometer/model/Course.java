package nl.patrickdevelopment.studiebarometer.model;

import java.io.Serializable;

/**
 * Created by Patrick on 2-3-2016.
 */
public class Course implements Serializable {

    public String name = "";
    public Integer ects = 1;
    public String grade = "";
    public Integer period = 1;
    public Integer chance = 1;


    public Course(String courseName, Integer ects, String grade, Integer period,int chance){
        this.name = courseName;
        this.ects = ects;
        this.grade = grade;
        this.period = period;
        this.chance = chance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEcts() {
        return ects;
    }

    public void setEcts(Integer ects) {
        this.ects = ects;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public boolean getCompleet() {
        return Double.parseDouble(this.grade) > 5.5;
    }

    public Integer getChance() {
        return chance;
    }

    public void setChance(Integer chance) {
        this.chance = chance;
    }
}
