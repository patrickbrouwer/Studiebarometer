package nl.patrickdevelopment.studiebarometer;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import junit.framework.Test;

import java.util.ArrayList;

import nl.patrickdevelopment.studiebarometer.controller.CourseController;

public class Overzicht_EC extends AppCompatActivity {

    private PieChart mChart;
    private TextView TVAdvice;
    private SharedPreferences SP;
    public static final int MAX_ECTS = 60;
    public int currentEcts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafische_interface);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mChart = (PieChart) findViewById(R.id.chart);
        mChart.setDescription("");
        mChart.setTouchEnabled(false);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleColor(Color.rgb(130, 130, 130));
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        TVAdvice = (TextView) findViewById(R.id.TVAdvice);
        final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        CourseController courseController = new CourseController(getApplicationContext());
        int TestData = courseController.getTotalETC();
        setData(TestData);

        //advies maken
        //In periode 1 kunnen er 13 ECTS gehaald worden
        //In periode 2 kunnen er 16 ECTS gehaald worden
        //In periode 3 kunnen er 14 ECTS gehaald worden
        //In periode 4 kunnen er 17 ECTS gehaald worden


        //<40 BSA
        //<50 blijft zitten
        //60 P gehaald

        //kernvakken (INET IOPR1 IOPR2 en IRDB)
        //minimaal 2 halen
        //3 niet gehaald = BSA

        //------------------------------------------------------------------------------------
        // periode 1
        // > 7
        // IOPR1
        // positief: kernvak behaald, boven 4 ec
        Log.e("completed kern",String.valueOf(courseController.getCompletedKernCourses()));

        int kernCourseCount = courseController.getCompletedKernCourses();

        if(courseController.getTotalETC() >= 7 && SP.getInt("PeriodArrayLocation", 0) == 0 && kernCourseCount >= 1 ) {
            TVAdvice.setText("Goed zo, ga zo door");
        } else if(courseController.getTotalETC() > 0 && SP.getInt("PeriodArrayLocation", 0) == 0) {
            TVAdvice.setText("Gaat goed, maar nog wel doorzetten");
        } else {
            TVAdvice.setText("Geen goede start van dit jaar, hard doorwerken!");
        }


        // periode 2
        // > 16 (haalbaar)
        // IRDB, INET
        // positief: totaal 20 hebben voor positief, 2 kernvakken behaald
        if(courseController.getTotalETC() >= 20 && SP.getInt("PeriodArrayLocation", 0) == 1 && kernCourseCount >= 2) {
            TVAdvice.setText("Goed zo, ga zo door");
        } else if(courseController.getTotalETC() >= 10 && SP.getInt("PeriodArrayLocation", 0) == 1 && kernCourseCount >= 1) {
            TVAdvice.setText("Gaat goed, maar nog wel doorzetten");
        } else if(SP.getInt("PeriodArrayLocation", 0) == 1){
            TVAdvice.setText("Je maakt het je zelf wel heel lastig");
        }

        // periode 3
        // > 9 EC (haalbaar)
        // IOPR 2
        // positief: 30 studiepunten ,2 kernvakken behaald
        if(courseController.getTotalETC() >= 30 && SP.getInt("PeriodArrayLocation", 0) == 2 && kernCourseCount >= 2) {
            TVAdvice.setText("Goed zo, ga zo door");
        } else if(courseController.getTotalETC() >= 25 && SP.getInt("PeriodArrayLocation", 0) == 2 && kernCourseCount >= 2){
            TVAdvice.setText("Gaat goed, maar nog wel doorzetten");
        } else if(SP.getInt("PeriodArrayLocation", 0) == 2){
            TVAdvice.setText("Je maakt het je zelf wel heel lastig");
        }

        // periode 4
        // > 10
        // IPXXXX
        // negatief: onder 40 ec,
        // gemiddeld: onder 50 ec
        // positief: boven 50 ec
        // geweldig: 60 behaald
        if(courseController.getTotalETC() > 40 && SP.getInt("PeriodArrayLocation", 0) == 3 && kernCourseCount > 2 ){
            TVAdvice.setText("Goed zo, ga zo door");
        } else if(courseController.getTotalETC() > 50 && SP.getInt("PeriodArrayLocation", 0) == 3 && kernCourseCount > 4) {
            TVAdvice.setText("Zeker over naar jaar 2!");
        }else if(courseController.getTotalETC() > 60 && SP.getInt("PeriodArrayLocation", 0) == 3 && kernCourseCount > 4) {
            TVAdvice.setText("Gefeliciteerd met je P!");
        } else if(SP.getInt("PeriodArrayLocation", 0) == 3) {
            TVAdvice.setText("Niet goed, dit wordt een BSA!");
        }

    }

    private void setData(int aantal) {
        currentEcts = aantal;
        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> xValues = new ArrayList<>();

        yValues.add(new Entry(aantal, 0));
        xValues.add("Behaalde ECTS");

        yValues.add(new Entry(60 - currentEcts, 1));
        xValues.add("Resterende ECTS");

        //  http://www.materialui.co/colors
        ArrayList<Integer> colors = new ArrayList<>();
        if (currentEcts <10) {
            colors.add(Color.rgb(244,81,30));
        } else if (currentEcts < 40){
            colors.add(Color.rgb(235,0,0));
        } else if  (currentEcts < 50) {
            colors.add(Color.rgb(253,216,53));
        } else {
            colors.add(Color.rgb(67,160,71));
        }

        colors.add(Color.rgb(255,0,0));


        PieDataSet dataSet = new PieDataSet(yValues, "ECTS");
        //  dataSet.setDrawValues(false); //schrijf ook de getallen weg.
        dataSet.setColors(colors);

        PieData data = new PieData(xValues, dataSet);
        mChart.setData(data);        // bind je dataset aan de chart.
        mChart.invalidate();        // Aanroepen van een volledige redraw
        Log.d("aantal =", ""+currentEcts);
    }

}
