package nl.patrickdevelopment.studiebarometer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.patrickdevelopment.studiebarometer.controller.CourseController;
import nl.patrickdevelopment.studiebarometer.database.DatabaseHelper;
import nl.patrickdevelopment.studiebarometer.model.Course;

public class CourseDetailActivity extends AppCompatActivity {

    static final String STATE_SUBJECT = "subject";
    private String subject;

    TextView TVName, TVPeriod, TVec, TVGrade, TVChance;
    Button BSave;
    EditText ETGrade;
    Course course;
    RequestQueue requestQueue;
    String insertUrl = "http://ipmedt.nl/imtpmd/api/vakken/post/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Get the saved instance
        super.onCreate(savedInstanceState);

        // Set the activity layout
        setContentView(R.layout.activity_course_detail);

        // create the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Make sure you can return to the mainActivity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create a requestQueue for Volley (where you push or get JSON info)
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        // If we don't have an saved instance, we have to get the previous saved info
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
            } else {
                subject = extras.getString(STATE_SUBJECT);
            }
        } else {
            subject = (String) savedInstanceState.getSerializable(STATE_SUBJECT);
        }

        final CourseController courseController = new CourseController(getApplicationContext());
        List<Course> courses = courseController.getCourseByName(subject);
        course = courses.get(0);

        TVGrade     = (TextView) findViewById(R.id.TVgrade);
        TVec        = (TextView) findViewById(R.id.TVec);
        TVPeriod    = (TextView) findViewById(R.id.TVPeriod);
        TVName      = (TextView) findViewById(R.id.TVName);
        BSave       = (Button)   findViewById(R.id.BSave);
        ETGrade     = (EditText) findViewById(R.id.edittext_main_grade);
        TVChance    = (TextView) findViewById(R.id.textview_main_attempt_int);



        ETGrade.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2, 1)});
        TVName.setText  (course.getName());
        TVec.setText    (String.valueOf(course.getEcts()));
        TVPeriod.setText(String.valueOf(course.getPeriod()));
        TVGrade.setText (String.valueOf(course.getGrade()));
        TVChance.setText(String.valueOf(course.getChance()));

        // Set the page title
        setTitle(course.getName());


        BSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ETGrade.getText().toString().trim().length() > 0) {
                    //lets update!
                    if (course.chance < 4) {
                        course.setGrade(ETGrade.getText().toString());
                        course.setChance(course.getChance() + 1);
                        Log.d("set chance", course.getChance().toString());
                        courseController.UpdateCourse(course);
                        update_grade();
                    } else {
                        //@TODO show error
                    }


                }
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_SUBJECT, subject);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(outState);


    }

    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }

    }


    private void update_grade(){
        // To the server
        StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        String UserNum = SP.getString("userNumber", null);

                        if (response.toLowerCase().contains(UserNum)) {
                            Log.d("Response","'yay'");
                        } else if (response.toLowerCase().contains("Duplicate entry")) {
                            Log.d("Response", response);

                        } else {
                            Log.d("Response", response);
                        }
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ging wat fout", error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                final SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

                Map<String, String> params = new HashMap<String, String>();
                params.put("vak_name", course.getName());
                params.put("user_number", SP.getString("userNumber", null));
                params.put("grade_grade", ETGrade.getText().toString());
                params.put("grade_attempt", course.getChance() + 1 + "");
                return params;
            }

        };

        requestQueue.add(stringRequest);
    }
}


